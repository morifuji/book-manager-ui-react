import React, { useCallback, useEffect, useState } from "react";
import "./App.css";
import Quagga from "@ericblade/quagga2";
import axios from "axios";
import { Row, Card, Typography, notification, Button, Input } from "antd";
import { ApiOutlined, PlusSquareOutlined, ShrinkOutlined, UserOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";

type Book = {
  jan: string;
  title: string;
  subtitle: string;
  imageUrl: string;
};

function App() {
  const width = document.body.clientWidth as number;
  const height = document.body.clientHeight as number;

  const [email, setEmail] = useState("");
  const [apiServerUrl, setApiServerUrl] = useState("http://localhost:3000/books");

  const [bookMap, setBookMap] = useState<Map<string, Book>>(new Map());
  const [alredyLoadedIsbnSet, setAlredyLoadedIsbnSet] = useState<Set<string>>(
    new Set()
  );
  
  const [isDisplayBarcodeReader, setIsDisplayBarcodeReader] = useState(true);

  const fetchBookData = useCallback(async (barcodeStr: string) => {
    console.log(barcodeStr)
    const { data } = await axios.get(
      `https://www.googleapis.com/books/v1/volumes?q=isbn:${barcodeStr}`
    );
    if (data.totalItems === 0) return;
    const responseBook = data.items[0].volumeInfo;

    notification.info({
      message: "書籍を読み取りました",
      description: responseBook.title,
    });
    setBookMap((bookMap) => {
      bookMap.set(barcodeStr, {
        jan: barcodeStr,
        title: responseBook.title,
        subtitle: responseBook.subtitle,
        imageUrl: responseBook.imageLinks?.thumbnail,
      });
      return new Map(bookMap.entries());
    });
  }, []);

  const onDetectCode = useCallback(async (code: string|null)=>{
      const isIsbn = (code: string) => ["978", "979"].includes(code.substring(0, 3))

      console.log("alredyLoadedIsbnSet", alredyLoadedIsbnSet)
      if (code === null || alredyLoadedIsbnSet.has(code) || !isIsbn(code)) return;

      // WARNING: setStateをarrow関数で定義すると即時的に処理されず、大量にfetchBookDataが走ってしまうためarrow関数で定義しない
      alredyLoadedIsbnSet.add(code);
      setAlredyLoadedIsbnSet(new Set(alredyLoadedIsbnSet.values()))
      await fetchBookData(code);
  }, [alredyLoadedIsbnSet])

  useEffect(() => {
    Quagga.init(
      {
        inputStream: {
          name: "Live",
          type: "LiveStream",
          target: "#scanner",
          constraints: {
            width: width - 50,
            height: height - 50,
          },
          area: {
            top: "10%",
            right: "10%",
            left: "10%",
            bottom: "10%",
          },
        },
        decoder: {
          readers: ["ean_reader"],
        }
      },
      (err) => {
        if (err) {
          alert(err);
          return;
        }
        console.log("Initialization finished. Ready to start");
        Quagga.onDetected(async (data) => {
          onDetectCode(data.codeResult.code)
        });

        Quagga.start();
      }
    );
  }, []);

  const clear = useCallback(() => {
    setAlredyLoadedIsbnSet(new Set());
    setBookMap(new Map());
  }, []);

  const sendBook = useCallback(async (isbn: string) => {
    const book = bookMap.get(isbn);
    if (!book) return;
    await axios
      .post(apiServerUrl, {
        title: book.title,
        coverImageUrl: book.imageUrl,
        email: email,
      })
      .then(() => {
        notification.success({
          message: "書籍の登録が完了しました",
          description: book.title
        });
      })
      .catch((err) => {
        notification.error({
          message: "書籍の登録に失敗しました",
          description: book.title + "/" + err.message,
        });
      });
  }, [email, bookMap, apiServerUrl]);

  return (
    <div className="App">
      <Typography.Title level={3}>
        バーコードスキャナ
        <Button
        shape="circle"
          onClick={() => setIsDisplayBarcodeReader(!isDisplayBarcodeReader)}
          icon={<ShrinkOutlined />}
        >
        </Button>
      </Typography.Title>

      <div
        id="scanner"
        style={{ display: isDisplayBarcodeReader ? "block" : "none" }}
      ></div>

      <Typography.Title level={3}>読み取り済み書籍一覧</Typography.Title>
        <Input placeholder="メアド" value={email} onChange={e=>setEmail(e.target.value)} prefix={<UserOutlined />} />
        <Input placeholder="APIサーバーURL" value={apiServerUrl} onChange={e=>setApiServerUrl(e.target.value)} prefix={<ApiOutlined />} />
      <Button danger onClick={clear}>読み取り済み書籍をリセット</Button>
      
      <Row>
        {Array.from(bookMap.entries()).map(([key, book]) => (
          <div style={{ padding: "10px" }} key={key}>
            <Card
              hoverable
              style={{ width: 240 }}
              actions={[<PlusSquareOutlined onClick={(e) => sendBook(key)} />]}
              cover={<img alt="example" src={book.imageUrl} />}
            >
              <Card.Meta title={book.title} description={book.subtitle} />
            </Card>
          </div>
        ))}
      </Row>
    </div>
  );
}

export default App;
